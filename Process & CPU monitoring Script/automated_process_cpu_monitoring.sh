#!/bin/bash

#########################################################################################################
# Author: Vaibhav Satve                                                                                 #
# Email: vvaibhav53@gmail.com                                                                           #
#########################################################################################################
## This script List all the process running for current user and print out the processess on console.  ##
#########################################################################################################


USER=$(whoami)

read -p "Would you like to sort the processes output by memory or CPU? (m/c): " INPUT

if [ "$INPUT" == "m" ]
then
        echo "Below are all Running processes for user $USER"
        echo " " 
        ps aux --sort -rss | grep -i $USER
        echo " "
elif [ "$INPUT" == "c" ]
then
        echo "Below are the most CPU consumed processes for $USER"
        echo " "
        ps aux --sort -%cpu | grep -i $USER
        echo " "
else
        echo "Enter Valid Input, either m or c"
fi
