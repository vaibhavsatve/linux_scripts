#########################################################################################################
# Author: Vaibhav Satve                                                                                 #
# Email: vvaibhav53@gmail.com                                                                           #
#########################################################################################################
## This script List all the process running for current user and print out the processess on console.  ##
#########################################################################################################

You Can Run this script on Linux machine and it will list all the running process for the user and script will fetch the most CPU consumed processess

Instructions:

1. Excute the script with ./<script name>.sh
2. Enter the input (m or c):
                m = for collecting all the running processess for the current user
                c = To list all the processess which are consuming more CPU

Note: if you enter any other option, script will instruct you to choose either m / c as input.
