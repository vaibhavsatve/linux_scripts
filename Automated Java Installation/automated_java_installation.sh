#!/bin/bash

## This script will automatically detects if Java Version is installed or not on this machine and if Not installed, 
## then it will automatically detects appropriate Package Manager and install the correct Java Version and Echo's it !

## Author : Vaibhav Satve
## Email :  vvaibhav53@gmail.com
## Github : https://github.com/vaibhavSatve

JAVA_PACKAGE="java-1.8.0-openjdk"

if java -version > /dev/null 2>&1;
then
    echo "Java is already installed on this machine with version $(java -version)"
else
    echo "Java is not installed on this machine. Lets install it now !"

    # This IF block checks if APT package manager Available and if yes, then install the JAVA using apt-get

    if command -v apt-get > /dev/null 2>&1
    then
        echo "Using APT package Manager"
        sudo apt-get update -qq > /dev/null
        sudo apt-get install -qq -y $JAVA_PACKAGE > /dev/null

        # If the above command returns 0 success code then, Below IF block echo's the Status as successful
        # If the above command returns other than 0, then it echo's the Failed status

        if [[ "$?" -eq 0 ]]
        then
            echo "JAVA Installed Successfully"
        else
            echo "JAVA Installation Failed"
            exit 1
        fi

    # This Elif block will check if YUM package manager is available and if yes, then it installs the JAVA using YUM

    elif command -v yum > /dev/null 2>&1
    then
        echo "Using YUM Package Manager"
        sudo yum -q -y install $JAVA_PACKAGE > /dev/null

        ## If the above command returns 0 success code then, below if block will echo's status successful and failure for other than 0 status code

        if [[ "$?" -eq 0 ]]
        then
            echo "JAVA Installed Successfully"
        else
            echo "JAVA Installation Failed"
            exit 1
        fi
    else
        echo "No Suitable Package Manager Detected, hence aborting script"
        exit 1
    fi
fi

# Installed JAVA version is Echo'ed at the end of the script!
echo " "
echo " Installed JAVA Version is: "
java -version
