#########################################################################################################
# Author: Vaibhav Satve                                                                                 #
# Email: vvaibhav53@gmail.com                                                                           #
#########################################################################################################


###

The java_installation.sh script is intended to install java version on Linux machine.

The script will auto detect which package manger script should use and accordingly it installs the JAVA on the linux machine.

Once the JAVA installation is finish, script displays the installed java -version as well.

If the JAVA version is already installed and if you run the script, it will inform the same and it will show the installed java -version for you.

###
